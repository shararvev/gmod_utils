--[[
	Based on netvars 1.1 https://github.com/Chessnut/NutScript
	Copyright rebel1324 2017
--]]
-- luacheck: globals SERVER netvars netstream FindMetaTable hook isnumber

netvars = netvars or {}

local stored = netvars.stored or {}
netvars.stored = stored

local globals = netvars.globals or {}
netvars.globals = globals

netvars.multicastVars = netvars.multicastVars or {}

local ENTITY = FindMetaTable("Entity")
local PLAYER = FindMetaTable("Player")

-- shit
function ENTITY:InitNetVar(key, value)
	local idx = self:EntIndex()
	stored[idx] = stored[idx] or {}
	if stored[idx][key] ~= value then
		stored[idx][key] = value
	end
end

function netvars.GetNetVar(key)
	return globals[key]
end

-- return nothing if not set
function ENTITY:GetNetVar(key)
	local idx = self:EntIndex()
	if stored[idx] then
		return stored[idx][key]
	end
end

if SERVER then
	function netvars.SetNetVar(key, value)
		if globals[key] ~= value then
			globals[key] = value
			netstream.Start(nil, "_SetGlobalNetVar", key, value)
		end
	end

	local function isInf(num)
		return num == math.huge or num == -math.huge
	end

	local function isNaN(num)
		return isnumber(num) and num ~= num
	end

	local function SetNetVar(self, key, value, ply)
		if isInf(value) then
			print("NetVars error: attempt to use INF, clamp", key, value)
			debug.Trace()
			value = math.Clamp(value, -4294967295, 4294967295)
		end
		if isNaN(value) then
			print("NetVars error: attempt to use NAN, drop", key, value)
			debug.Trace()
			return
		end

		local idx = self:EntIndex()
		stored[idx] = stored[idx] or {}
		if stored[idx][key] ~= value then
			stored[idx][key] = value
			netstream.Start(ply, "_SetNetVar", idx, key, value)
		end
	end

	function ENTITY:SetNetVar(key, value, ply)
		SetNetVar(self, key, value, ply)
	end

	hook.Add(
		"EntityRemoved",
		"DeleteNetVars",
		function(ent)
			local idx = ent:EntIndex()
			if stored[idx] then
				stored[idx] = nil
				netstream.Start(nil, "_DeleteNetVars", idx)
			end
		end
	)

	function PLAYER:SetNetVar(key, value)
		if netvars.multicastVars[key] then
			SetNetVar(self, key, value)
		else
			SetNetVar(self, key, value, self)
		end
	end

	function PLAYER:SyncNetVars()
		netstream.Start(self, "_SyncNetVars", stored, globals)
	end

	hook.Add(
		"PlayerInitialSpawn",
		"SyncNetVars",
		function(ply)
			ply:SyncNetVars()
		end
	)
else
	local function topClear(t)
		for k in pairs(t) do
			t[k] = nil
		end
	end

	local function topMerge(t1, t2)
		for k, v in pairs(t2) do
			t1[k] = v
		end
	end

	netstream.Hook(
		"_SyncNetVars",
		function(_stored, _globals)
			topClear(stored)
			topClear(globals)
			topMerge(stored, _stored)
			topMerge(globals, _globals)
		end
	)

	netstream.Hook(
		"_SetNetVar",
		function(idx, key, value)
			stored[idx] = stored[idx] or {}
			stored[idx][key] = value
		end
	)

	netstream.Hook(
		"_DeleteNetVars",
		function(idx)
			stored[idx] = nil
		end
	)

	netstream.Hook(
		"_SetGlobalNetVar",
		function(key, value)
			globals[key] = value
		end
	)
end
