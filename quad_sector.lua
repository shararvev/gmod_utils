--[[
	Quad Sector - математический класс квадратных секторов.

	Представим что некое пространство разбито на прямоугольные параллелепипеды, вытянутые по высоте бесконечно далеко.
	Можно сказать что внутри определенного сектора может находится объект.
	С помощью этого класса мы можем узнать какому сектору принадлежит объект по абсолютным координатам.
	Также можем узнать позицию сектора по его id и получить все соседние сектора.

	В реальности этот класс не является хранилищем объектов или секторов. Все что делает этот класс, это расчеты.
	Объект хранит только параметры для расчетов, и предоставляет набор математических функций.

	Приоритет нумерации выбран по осям, сначала по x, затем по y, затем по z.
	Нумерация секторов и их позиции начинаются с 0, это позволяет избавиться от лишних математических операций.
	Позиция попадает в сектор по нижней границе, поэтому правый верхний угол это необрабатываемый случай.
--]]
-- luacheck: globals QuadSector netstream SERVER pon Vector

local floor = math.floor

local function CalcId(x, y, sideCount)
	return x + y * sideCount
end

local function CalcNearIds(x, y, sideCount, nearXY)
	local ids = {}
	for dy = -nearXY, nearXY do
		for dx = -nearXY, nearXY do
			local numX = x + dx
			local numY = y + dy
			if numX >= 0 and numX < sideCount and numY >= 0 and numY < sideCount then
				local id = CalcId(numX, numY, sideCount)
				ids[id] = true
			end
		end
	end
	return ids
end

local function CalcPos(id, sideCount)
	local y = floor(id / sideCount)
	local x = id - y * sideCount
	return Vector(x, y, 0)
end

local QUAD_SECTOR = {}
QUAD_SECTOR.__index = QUAD_SECTOR

function QuadSector(size, step, offset, nearXY)
	offset = offset or Vector()
	nearXY = nearXY or 1
	local grid = {
		_step = step,
		_offset = offset,
		_sideCount = math.ceil(size / step),
		_nearXY = nearXY
	}
	return setmetatable(grid, QUAD_SECTOR)
end

-- Возвращает id сектора по абсолютной позиции.
-- При выходе за диапазон возвращает ничего.
function QUAD_SECTOR:GetId(pos)
	local sideCount = self._sideCount
	local step = self._step
	local num = pos - self._offset
	local numX, numY = num:Unpack()
	numX = floor(numX / step)
	numY = floor(numY / step)
	if numX >= 0 and numX < sideCount and numY >= 0 and numY < sideCount then
		return CalcId(numX, numY, sideCount)
	end
end

-- Возвращает таблицу соседних секторов. В таблице id записаны ключами.
-- При выходе за диапазон возвращает прилегающие к границе сектора или пустую таблицу.
function QUAD_SECTOR:GetNearIds(pos)
	local sideCount = self._sideCount
	local step = self._step
	local num = pos - self._offset
	local numX, numY = num:Unpack()
	numX = floor(numX / step)
	numY = floor(numY / step)
	return CalcNearIds(numX, numY, sideCount, self._nearXY)
end

-- Возвращает позицию нижней границы сектора.
-- При выходе за диапазон возвращает ничего.
function QUAD_SECTOR:GetPos(id)
	local sideCount = self._sideCount
	if id >= 0 and id < sideCount * sideCount then
		local pos = CalcPos(id, sideCount)
		pos:Mul(self._step)
		pos:Add(self._offset)
		return pos
	end
end

-- Возвращает центр сектора.
-- При выходе за диапазон возвращает ничего.
function QUAD_SECTOR:GetCenterPos(id)
	local sideCount = self._sideCount
	if id >= 0 and id < sideCount * sideCount then
		local pos = CalcPos(id, sideCount)
		pos:Add(Vector(0.5, 0.5, 0))
		pos:Mul(self._step)
		pos:Add(self._offset)
		return pos
	end
end

-- Возвращает таблицу секторов из to, которых нет в from. В таблице id записаны ключами.
function QUAD_SECTOR:DiffIds(from, to)
	local diff = {}
	for id in pairs(to) do
		if not from[id] then
			diff[id] = true
		end
	end
	return diff
end
