--[[
	Based on netstream 2.1.0 https://github.com/alexgrist/NetStream
	Copyright alexgrist 2015

	Размер пакета 64 кб, где 256 байт зарезервированы для имени хука и служебной информации.
	Подразделения на большие сообщения нет, потому что формирование 1-го пакета происходит мгновенно.
	Данные серилизуются с помощью pon.
	Данные сжимаются если их размер больше минимальной длинны "эффективного сжатия".
	Максимальный размер сообщения в коде ограничен числом пакетов, это примерно 16 мб.

	Пока нет эффективного способа проверять пакеты от клиента, поэтому на сервере установлен SILENT_ON_SERVER,
	чтобы не вызывать ассерт и не спамить в консоль.

	Скорость передачи определяется net_maxfragments вызывающемся 20 раз в секунду. Не зависит от тикрейта.
	По умолчанию равна 1260 что пример 24 кб/с.	Можно поднять до 1792 и получить 35 кб/с. net_splitrate не влияет.
	net_splitpacket_maxrate определяет сколько сплитить пакетов, но это лишь столит сервер при меньших значениях.
--]]
-- luacheck: globals netstream SERVER pon Stack

local net = net
local ErrorNoHalt = ErrorNoHalt
local pairs = pairs
local pcall = pcall
local type = type
local util = util

if SERVER then
	util.AddNetworkString("NetStreamDS")
end

netstream = netstream or {}

local stored = netstream.stored or {}
netstream.stored = stored

local cache = netstream.cache or {}
netstream.cache = cache

local SILENT_ON_SERVER = true

local COMPRESS_EFFICIENCY = 128
-- max possible message size controlled by PACKETS_BITS is ~ 16 mb
local PACKETS_BITS = 8
local BUFFER_BITS = 16
local BUFFER_SIZE = 65536 - 256

-- A function to split data for a data stream.
function netstream.Split(data)
	local slices = Stack()

	while #data > BUFFER_SIZE do
		slices:Push(string.sub(data, 1, BUFFER_SIZE))
		data = string.sub(data, BUFFER_SIZE + 1)
	end
	slices:Push(data)

	return slices
end

-- A function to hook a net stream.
function netstream.Hook(name, callback)
	stored[name] = callback
end

-- A function to start a net stream.
function netstream.Start(ply, name, ...)
	if SERVER then
		ply = ply or player.GetAll()
	end

	local compressed = false
	local data = pon.encode({...})
	if #data > COMPRESS_EFFICIENCY then
		local lzma = util.Compress(data)
		if isstring(lzma) then
			compressed = true
			data = lzma
		end
	end

	local packetsStack = netstream.Split(data)
	local packetsCount = packetsStack:Size()

	for i, pack in ipairs(packetsStack) do
		local packLength = #pack

		net.Start("NetStreamDS")
		net.WriteString(name)
		net.WriteBool(compressed)
		net.WriteUInt(packetsCount, PACKETS_BITS)
		net.WriteUInt(i, PACKETS_BITS)
		net.WriteUInt(packLength, BUFFER_BITS)
		net.WriteData(pack, packLength)
		if SERVER then
			net.Send(ply)
		else
			net.SendToServer()
		end
	end
end

if not SERVER then
	netstream._Start = netstream._Start or netstream.Start
	function netstream.Start(name, ...)
		netstream._Start(nil, name, ...)
	end
end

local function NetStreamReceive(_, ply, name)
	name = name or net.ReadString() or "Invalid stream"
	local compressed = net.ReadBool()
	local packetsCount = net.ReadUInt(PACKETS_BITS)
	local packNumber = net.ReadUInt(PACKETS_BITS)
	local packLength = net.ReadUInt(BUFFER_BITS)
	local pack = net.ReadData(packLength)

	if SERVER and SILENT_ON_SERVER then
		if not stored[name] then
			hook.Run("NetStreamUnlisted", ply, name)
			print("Lua Error: NetStream, netstream.lua", ply, compressed, packetsCount, packNumber, packLength, pack)
			ErrorNoHalt("NetStream: NetStreamUnlisted '" .. name .. "'\n")
			return
		end
	else
		assert(stored[name], "Attempt to receive packets for unlisted hook!")
	end

	cache[name] = cache[name] or {}
	cache[name][packNumber] = pack

	if packNumber == packetsCount then
		if SERVER and SILENT_ON_SERVER then
			if #cache[name] > packetsCount then
				-- drop trash packets by other player
				for i = packetsCount + 1, #cache[name] do
					cache[name][i] = nil
				end
			end
		else
			assert(#cache[name] == packetsCount, "Cache size must be equal to number of packets!")
		end

		local bStatus1, data = pcall(table.concat, cache[name])
		if bStatus1 then
			if compressed then
				data = util.Decompress(data)
			end
		elseif SERVER then
			print("Lua Error: NetStream, netstream.lua", ply, compressed, packetsCount, packNumber, packLength, pack)
			ErrorNoHalt("NetStream: '" .. name .. "'\n" .. data .. "\n")
			cache[name] = nil
			return
		end

		local bStatus, value = pcall(pon.decode, data)
		if bStatus then
			if SERVER then
				stored[name](ply, unpack(value))
			else
				stored[name](unpack(value))
			end
		else
			if SERVER then
				print("Lua Error: NetStream, netstream.lua", ply, compressed, packetsCount, packNumber, packLength, pack)
			end
			ErrorNoHalt("NetStream: '" .. name .. "'\n" .. value .. "\n")
		end

		cache[name] = nil
	end
end
net.Receive("NetStreamDS", NetStreamReceive)
